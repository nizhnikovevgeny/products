
package GUI;

import java.awt.*;
import java.awt.event.*;
import javaapplication1.JavaApplication1;
import javax.swing.*;


public class GUI extends JFrame{
    private JLabel label = new JLabel("Input: ");
    private JTextField input = new JTextField("",20);
    private JButton button = new JButton("Enter");
    
    public GUI(){       
        this.setBounds(200,200,500,500);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(label);
        panel.add(input);
        panel.add(button);
        setContentPane(panel);
        button.addActionListener(new ActionListener(){
             @Override
        public void actionPerformed(ActionEvent e) {
            
            JOptionPane.showMessageDialog(null,input.getText());
//            JavaApplication1.Answer = input.getText();
        }
        });
    }
    

    public String getMessege(){
        return input.getText();
}
}