/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1.products;
/**
 *
 * @author b17-901
 */
public class Products {
    private float belki,zhiri,uglevodi,cennost;
    private String name;
    private void setName(String name) {
        this.name=name;
    }
    private void setBelki(float belki) {
        this.belki=belki;
    } 
    private void setZhiri(float zhiri) {
        this.zhiri=zhiri;
    } 
    private void setUglevodi(float uglevodi) {
        this.uglevodi=uglevodi;
    } 
    
   public Products(String name,float belki, float zhiri, float uglevodi) {
       this.setBelki(belki);
       this.setZhiri(zhiri);
       this.setUglevodi(uglevodi);
       this.countCennost(belki,zhiri,uglevodi);
       this.setName(name);
    }
    
    private void countCennost(float belki, float zhiri, float uglevodi) {
        this.cennost = (zhiri*9.29f+belki*4.1f+uglevodi*4.1f);
    }
    
    public float getCennost() {
        return this.cennost;
    }
    public float getBelki() {
        return this.belki;
    }
    public float getZhiri() {
        return this.zhiri;
    }
    public float getUglevodi() {
        return this.uglevodi;
    }
     public String getName() {
        return this.name;
    }
    
}