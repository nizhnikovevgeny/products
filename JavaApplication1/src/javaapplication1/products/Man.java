/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1.products;
import Exceptions.koefAException;
/**
 *
 * @author b17-901
 */
public class Man{
  private String name,sex;
  private float height, weight, age,  DCI, koefA,koefSex,eaten;  
  private int A,quantity;
  private void setName(String name) {
      this.name = name;
  }
  private void setSex(String sex) {
      this.sex = sex;
  }
  private void setHeight(float height) {
      this.height=height;
  }
  private void setWeight(float weight) {
      this.weight=weight;
  }
  private void setAge(float age) {
      this.age=age;
  }
  private void setA(int A) {
      this.A=A;
  }
  public void setQuantity(int quantity) {
      this.quantity = quantity;
  }
  public void setEaten(float eaten) {
      this.eaten=eaten;
  }
  private void countDCI() {
      this.DCI=this.koefA*(this.weight*10f+this.height*6.25f-this.age*5f+this.koefSex);
  }
  public Man(String name,String sex,float height,float weight,float age,int A) throws koefAException{
      this.setName(name);
      this.setHeight(height);
      this.setWeight(weight);
      this.setA(A);
      this.setAge(age); 
      this.setSex(sex.toLowerCase());
      this.setQuantity(0);
      this.setEaten(0);
    /*  if ((this.A<1)&&(this.A>7)) {
          this.setA(1);
      }
    */
    try{
      switch (this.A) {          
         case 1 :
             this.koefA=1.2f;
             break;
             case 2 :
             this.koefA=1.38f;
             break;
             case 3 :
             this.koefA=1.46f;
             break;
             case 4 :
             this.koefA=1.55f;
             break;
             case 5 :
             this.koefA=1.64f;
             break;
             case 6 :
             this.koefA=1.73f;
             break;
             case 7 :
             this.koefA=1.9f;
             break;              
             default :
             throw new koefAException(name);
     }
    } catch (koefAException e){
        this.A=1;
        this.koefA=1.2f;
    }
   
      if (this.sex.equals("male")) {
          this.koefSex=5f;
      } else {
          this.koefSex=-161f;
      }
      this.countDCI();
  
  }
    public String getName(){
      return this.name;
  }
    public float getHeight (){
        return this.height;
    }
    public float getWeight (){
        return this.weight;
    }
    public float getAge (){
        return this.age;
    }   
    public float getA (){
        return this.A;
    }
    public float getDCI(){
        return this.DCI;
    }  
    public int getQuantity(){
        return this.quantity;
    } 
    public float getEaten(){
        return this.eaten;
    } 

    public String getSex() {
        return sex;
    }
}
